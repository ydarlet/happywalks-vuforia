﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class button1_anim : MonoBehaviour, IVirtualButtonEventHandler {

	public GameObject button1;
	public Animator anim1;

	// Initialization
	void Start () {
		button1 = GameObject.Find ("button1");
		button1.GetComponent<VirtualButtonBehaviour> ().RegisterEventHandler (this);
		anim1.GetComponent<Animator> ();
	}

	public void OnButtonPressed(VirtualButtonBehaviour vb) {
		anim1.Play ("rotate_animation1");
		Debug.Log ("BTN pressed");
	}

	public void OnButtonReleased(VirtualButtonBehaviour vb) {
		anim1.Play ("none");
		Debug.Log ("BTN released");
	}

	// Update is called once per frame
	void Update () {
		
	}
}
